# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%      | N         |


---

### 筆刷

-我的筆刷用canvas內建的函數實作，有lineto()、moveto()、beginpath()、closepath()、stroke()等等

-按下筆刷按鍵之後就可以在空白處畫畫，底下有一個range bar可以調整筆畫粗細

### 橡皮擦

-我的筆刷用canvas內建的函數實作，用destination_out和arc()來完成

-按下筆刷按鍵之後就可以擦除筆跡，底下有一個range bar可以調整擦除粗細

### 圓形

-點選圓圈圖案在空白處拖曳出即可調整大小

-我用arc來實作圓形，而圓心則是根據滑鼠按下的點和當前滑鼠的位置算出終點即是圓心

### 三角形

-點選三角形圖案在空白處拖曳出即可調整大小

-我用moveto()、lineto()來實作三角形，根據滑鼠按下的點當作三角形的頂點和當前滑鼠的位置的y軸差即是高、左右x軸則是根據滑鼠按下原點的x軸和

-當前滑鼠的x軸用特定比例去決定一個等腰三角形的底
### 方形

-點選正方形圖案在空白處拖曳出即可調整大小

-我用rect()來畫方形，以滑鼠按下的點當方形的頂點，再用當前滑鼠的x,y座標分別跟頂點做運算，算出方形的長跟寬

### Redo&Undo

-我用三個array來存現在、之前、和之後的圖，當滑鼠放開代表筆畫完成，這時候undo就會儲存還沒完成筆畫的圖，present就會除存現在的圖，當undo的

-時候，present array 會pop一個圖push進redo array，undo array會pop一個圖push進present array，每次做redo、undo都會pop出一個canvas的存檔轉

-成圖片後在用drawimage畫上原本的canvas上

### 上傳檔案

-我用input file來上傳，當選擇檔案之後，就會把該圖片檔轉成url上傳然後創造新的image element再把url存進image轉成圖片並透過drawimage()畫到畫

-布上

### 下載檔案

-點選下載圖按鍵之後，就會把當前的canvas轉成圖片並且下載下來

### reset

-我用clearrect()來清除畫布上的元素，按下reset之後會清除畫布上的所有東西

### 筆刷顏色

-我用input color來選擇顏色，再把顏色資料儲存傳給筆刷的顏色函數

### 文字
-我使用createElement()來做，當滑鼠在canvas上點擊之後，就會產生一個input，輸入文字之後按下enter，就可以寫上canvas，文字按鈕下面有兩個選單，
-可以選擇字形和大小，並且改變筆刷顏色的選單也可以用來改變字體的顏色

### PageLink:

https://107062301.gitlab.io/AS_01_WebCanvas

<style>
table th{
    width: 100%;
}
</style>
